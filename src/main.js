import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router';

import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

const router = new VueRouter({
  mode:   'history',
  routes: [
    {
      path: '*',
      name: 'index',
      component: () => import('@/views/index.vue')
    }
  ]
});


Vue.config.productionTip = false
Vue.use(ViewUI);
Vue.use(VueRouter);
Vue.use(Antd);
// 添加 eventbus
Vue.prototype.$bus = new Vue();

/**
 * 应用入口
 */
window.$app = new Vue({
  el:     '#app',
  render: h => h(App),
  router,
});

// 阻止双击放大
let lastTouchEnd = 0;

document.addEventListener('touchstart', function (event) {
  if (event.touches.length > 1) {
    event.preventDefault();
  }
});
document.addEventListener('touchend', function (event) {
  const now = (new Date()).getTime();

  if (now - lastTouchEnd <= 300) {
    event.preventDefault();
  }
  lastTouchEnd = now;
}, false);

// 阻止双指放大
document.addEventListener('gesturestart', function (event) {
  event.preventDefault();
});
